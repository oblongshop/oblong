All our wall art prints and frames are exclusive to our store. We are a little different to other online wall art stores who mostly sell the same stock images sourced from online image libraries. Oblong only sells original, limited run, quality photographic prints taken by our esteemed photographer Seus Ku.

Website : https://oblongshop.com/